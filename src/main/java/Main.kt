import javax.swing.SwingUtilities.invokeLater

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        invokeLater { JOCLSimpleMandelbrot(1024, 768) }
    }
}