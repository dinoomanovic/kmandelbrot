import org.jocl.*
import org.jocl.CL.*
import java.awt.*
import java.awt.event.MouseEvent
import java.awt.event.MouseMotionListener
import java.awt.image.BufferedImage
import java.awt.image.DataBufferInt
import java.io.BufferedReader
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader
import javax.swing.JComponent
import javax.swing.JFrame
import javax.swing.JPanel

/**
 * The image which will contain the Mandelbrot pixel data
 */
private var image: BufferedImage? = null

/**
 * The width of the image
 */
private var sizeX = 0

/**
 * The height of the image
 */
private var sizeY = 0

/**
 * The component which is used for rendering the image
 */
private var imageComponent: JComponent? = null

/**
 * The OpenCL context
 */
private var context: cl_context? = null

/**
 * The OpenCL command queue
 */
private var commandQueue: cl_command_queue? = null

/**
 * The OpenCL kernel which will actually compute the Mandelbrot
 * set and store the pixel data in a CL memory object
 */
private var kernel: cl_kernel? = null

/**
 * The OpenCL memory object which stores the pixel data
 */
private var pixelMem: cl_mem? = null

/**
 * An OpenCL memory object which stores a nifty color map,
 * encoded as integers combining the RGB components of
 * the colors.
 */
private var colorMapMem: cl_mem? = null

/**
 * The color map which will be copied to OpenCL for filling
 * the PBO.
 */
private var colorMap: IntArray? = null

/**
 * The minimum x-value of the area in which the Mandelbrot
 * set should be computed
 */
private var x0 = -2f

/**
 * The minimum y-value of the area in which the Mandelbrot
 * set should be computed
 */
private var y0 = -1.3f

/**
 * The maximum x-value of the area in which the Mandelbrot
 * set should be computed
 */
private var x1 = 0.6f

/**
 * The maximum y-value of the area in which the Mandelbrot
 * set should be computed
 */
private var y1 = 1.3f

/**
 * Creates the JOCLSimpleMandelbrot sample with the given
 * width and height
 */
fun JOCLSimpleMandelbrot(width: Int, height: Int) {
    sizeX = width
    sizeY = height

    // Create the image and the component that will paint the image
    image = BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_RGB)
    imageComponent = object : JPanel() {
        public override fun paintComponent(g: Graphics) {
            super.paintComponent(g)
            g.drawImage(image, 0, 0, this)
        }
    }

    // Initialize the mouse interaction
    initInteraction()

    // Initialize OpenCL
    initCL()

    // Initial image update
    updateImage()

    // Create the main frame
    val frame = JFrame("JOCL Simple Mandelbrot")
    frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
    frame.layout = BorderLayout()
    (imageComponent as JPanel).preferredSize = Dimension(width, height)
    frame.add(imageComponent, BorderLayout.CENTER)
    frame.pack()

    frame.isVisible = true
}

/**
 * Initialize OpenCL: Create the context, the command queue
 * and the kernel.
 */
private fun initCL() {
    val platformIndex = 0
    val deviceType = CL_DEVICE_TYPE_ALL
    val deviceIndex = 0

    // Enable exceptions and subsequently omit error checks in this sample
    CL.setExceptionsEnabled(true)

    // Obtain the number of platforms
    val numPlatformsArray = IntArray(1)
    clGetPlatformIDs(0, null, numPlatformsArray)
    val numPlatforms = numPlatformsArray[0]

    // Obtain a platform ID
    val platforms = arrayOfNulls<cl_platform_id>(numPlatforms)
    clGetPlatformIDs(platforms.size, platforms, null)
    val platform = platforms[platformIndex]

    // Initialize the context properties
    val contextProperties = cl_context_properties()
    contextProperties.addProperty(CL_CONTEXT_PLATFORM.toLong(), platform)

    // Obtain the number of devices for the platform
    val numDevicesArray = IntArray(1)
    clGetDeviceIDs(platform, deviceType, 0, null, numDevicesArray)
    val numDevices = numDevicesArray[0]

    // Obtain a device ID
    val devices = arrayOfNulls<cl_device_id>(numDevices)
    clGetDeviceIDs(platform, deviceType, numDevices, devices, null)
    val device = devices[deviceIndex]

    // Create a context for the selected device
    context = clCreateContext(
            contextProperties, 1, device?.let { arrayOf(it) }, null, null, null)

    // Create a command-queue for the selected device
    commandQueue = clCreateCommandQueue(context, device, 0, null)

    // Program Setup
    val source = readFile("kernels/SimpleMandelbrot.cl")

    // Create the program
    val cpProgram = clCreateProgramWithSource(context, 1,
            arrayOf(source), null, null)

    // Build the program
    clBuildProgram(cpProgram, 0, null, "-cl-mad-enable", null, null)

    // Create the kernel
    kernel = clCreateKernel(cpProgram, "computeMandelbrot", null)

    // Create the memory object which will be filled with the
    // pixel data
    pixelMem = clCreateBuffer(context, CL_MEM_WRITE_ONLY,
            (sizeX * sizeY * Sizeof.cl_uint).toLong(), null, null)

    // Create and fill the memory object containing the color map
    initColorMap(256, Color.GRAY, Color.ORANGE, Color.CYAN)
    colorMapMem = clCreateBuffer(context, CL_MEM_READ_WRITE,
            (colorMap!!.size * Sizeof.cl_uint).toLong(), null, null)
    clEnqueueWriteBuffer(commandQueue, colorMapMem, true, 0,
            (colorMap!!.size * Sizeof.cl_uint).toLong(), Pointer.to(colorMap!!), 0, null, null)
}

/**
 * Helper function which reads the file with the given name and returns
 * the contents of this file as a String. Will exit the application
 * if the file can not be read.
 *
 * @param fileName The name of the file to read.
 * @return The contents of the file
 */
private fun readFile(fileName: String): String? {
    try {
        val br = BufferedReader(
                InputStreamReader(FileInputStream(fileName)))
        val sb = StringBuffer()
        var line: String?
        while (true) {
            line = br.readLine()
            if (line == null) {
                break
            }
            sb.append(line).append("\n")
        }
        return sb.toString()
    } catch (e: IOException) {
        e.printStackTrace()
        System.exit(1)
        return null
    }

}

/**
 * Creates the colorMap array which contains RGB colors as integers,
 * interpolated through the given colors with colors.length * stepSize
 * steps
 *
 * @param stepSize The number of interpolation steps between two colors
 * @param colors The colors for the map
 */
private fun initColorMap(stepSize: Int, vararg colors: Color) {
    colorMap = IntArray(stepSize * colors.size)
    var index = 0
    for (i in 0 until colors.size - 1) {
        val c0 = colors[i]
        val r0 = c0.red
        val g0 = c0.green
        val b0 = c0.blue

        val c1 = colors[i + 1]
        val r1 = c1.red
        val g1 = c1.green
        val b1 = c1.blue

        val dr = r1 - r0
        val dg = g1 - g0
        val db = b1 - b0

        for (j in 0 until stepSize) {
            val alpha = j.toFloat() / (stepSize - 1)
            val r = (r0 + alpha * dr).toInt()
            val g = (g0 + alpha * dg).toInt()
            val b = (b0 + alpha * db).toInt()
            val rgb = r shl 16 or
                    (g shl 8) or
                    (b shl 0)
            colorMap!![index++] = rgb
        }
    }
}


/**
 * Attach the mouse- and mouse wheel listeners to the glComponent
 * which allow zooming and panning the fractal
 */
private fun initInteraction() {
    val previousPoint = Point()

    imageComponent?.addMouseMotionListener(object : MouseMotionListener {
        override fun mouseDragged(e: MouseEvent) {
            val dx = previousPoint.x - e.x
            val dy = previousPoint.y - e.y

            val wdx = x1 - x0
            val wdy = y1 - y0

            x0 += dx / 150.0f * wdx
            x1 += dx / 150.0f * wdx

            y0 += dy / 150.0f * wdy
            y1 += dy / 150.0f * wdy

            previousPoint.setLocation(e.x, e.y)

            updateImage()
        }

        override fun mouseMoved(e: MouseEvent) {
            previousPoint.setLocation(e.x, e.y)
        }

    })

    imageComponent?.addMouseWheelListener { e ->
        val dx = x1 - x0
        val dy = y1 - y0
        val delta = e.wheelRotation / 20.0f
        x0 += delta * dx
        x1 -= delta * dx
        y0 += delta * dy
        y1 -= delta * dy

        updateImage()
    }
}


/**
 * Execute the kernel function and read the resulting pixel data
 * into the BufferedImage
 */
private fun updateImage() {
    // Set work size and execute the kernel
    val globalWorkSize = LongArray(2)
    globalWorkSize[0] = sizeX.toLong()
    globalWorkSize[1] = sizeY.toLong()

    val maxIterations = 250
    clSetKernelArg(kernel, 0, Sizeof.cl_mem.toLong(), Pointer.to(pixelMem!!))
    clSetKernelArg(kernel, 1, Sizeof.cl_uint.toLong(), Pointer.to(intArrayOf(sizeX)))
    clSetKernelArg(kernel, 2, Sizeof.cl_uint.toLong(), Pointer.to(intArrayOf(sizeY)))
    clSetKernelArg(kernel, 3, Sizeof.cl_float.toLong(), Pointer.to(floatArrayOf(x0)))
    clSetKernelArg(kernel, 4, Sizeof.cl_float.toLong(), Pointer.to(floatArrayOf(y0)))
    clSetKernelArg(kernel, 5, Sizeof.cl_float.toLong(), Pointer.to(floatArrayOf(x1)))
    clSetKernelArg(kernel, 6, Sizeof.cl_float.toLong(), Pointer.to(floatArrayOf(y1)))
    clSetKernelArg(kernel, 7, Sizeof.cl_int.toLong(), Pointer.to(intArrayOf(maxIterations)))
    clSetKernelArg(kernel, 8, Sizeof.cl_mem.toLong(), Pointer.to(colorMapMem!!))
    clSetKernelArg(kernel, 9, Sizeof.cl_int.toLong(), Pointer.to(intArrayOf(colorMap!!.size)))

    clEnqueueNDRangeKernel(commandQueue, kernel, 2, null,
            globalWorkSize, null, 0, null, null)

    // Read the pixel data into the BufferedImage
    val dataBuffer = image?.raster?.dataBuffer as DataBufferInt
    val data = dataBuffer.data
    clEnqueueReadBuffer(commandQueue, pixelMem, CL_TRUE, 0,
            (Sizeof.cl_int * sizeY * sizeX).toLong(), Pointer.to(data), 0, null, null)

    imageComponent?.repaint()
}